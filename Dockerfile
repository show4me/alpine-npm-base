FROM show4me/node:lts-alpine3.17-npm-9.1.2-r0

WORKDIR /home
# copy packages.json
COPY . .
RUN apk update && apk upgrade
# echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
# echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
# apk add --no-cache \
#   chromium@edge \
#   nss@edge \
# freetype@edge \
# harfbuzz@edge \
# ttf-freefont@edge
RUN apk add npm=8.14.0-r0
RUN apk add git
RUN apk add openssh
RUN apk add wget


RUN npm run clear:prod
RUN npm audit fix

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
# ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
# Puppeteer v1.12.2 works with Chromium 73.
# RUN npm i puppeteer@0.11.0



# Recommended tag: show4me/node:10.9.0-alpine-npm-8.14.0-r0
